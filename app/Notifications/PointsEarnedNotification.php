<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PointsEarnedNotification extends Notification
{
    use Queueable;

    protected $points;

    public function __construct($points)
    {
        $this->points = $points;
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toArray($notifiable)
    {
        return [
            'user_id' => $this->points->idUsuario,
            'points' => $this->points->puntos,
        ];
    }
}
