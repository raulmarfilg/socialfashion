<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{

    private $seguidoresCount;
    private $seguidosCount;

    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->seguidoresCount = $resource->seguidores->count();
        $this->seguidosCount = $resource->seguidos->count();
    }
    

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'   => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'role_id' => $this->roles,
            'roles' => $this->roles,
            'seguidoresCount' => $this->seguidores->count(),
            'seguidosCount' => $this->seguidos->count(),
            'puntos' => $this->puntos,
            'created_at' => $this->created_at->toDateString()
        ];
    }
    

    /**
     * Get the value of seguidoresCount
     */ 
    public function getSeguidoresCount()
    {
        return $this->seguidoresCount;
    }

    /**
     * Set the value of seguidoresCount
     *
     * @return  self
     */ 
    public function setSeguidoresCount($seguidoresCount)
    {
        $this->seguidoresCount = $seguidoresCount;

        return $this;
    }

    /**
     * Get the value of seguidosCount
     */ 
    public function getSeguidosCount()
    {
        return $this->seguidosCount;
    }

    /**
     * Set the value of seguidosCount
     *
     * @return  self
     */ 
    public function setSeguidosCount($seguidosCount)
    {
        $this->seguidosCount = $seguidosCount;

        return $this;
    }
}
