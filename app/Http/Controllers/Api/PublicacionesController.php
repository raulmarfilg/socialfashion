<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// SE AÑADE EL NUEVO MODELO
use App\Models\Publicacion;
use App\Models\Producto;

class PublicacionesController extends Controller
{
    public function index()
    {
        if (auth()->check()) {
            $userId = auth()->user()->id;
        } else {
            $userId = null;
        }

        $publicaciones = Publicacion::with([
            'user.media',
            'megusta',
            'comentarios.usuario.media',
            'productos' => function ($query) {
                $query->withPivot('valoracion');
            },
            'clics' => function ($query) use ($userId) {
                if ($userId) {
                    $query->where('idUsuario', $userId);
                }
            }
        ])->get();

        $publicaciones->each(function ($publicacion) {
            if ($publicacion->hasMedia('images-publicaciones')) {
                $publicacion->imagen = $publicacion->getFirstMedia('images-publicaciones')->getUrl();
            }
        });

        return $publicaciones;
    }




    public function store(Request $request)
    {
        $request->validate([
            'idUsuario' => 'required',
            'dePago' => 'required',
            'foto' => 'required|image',
        ]);

        $publi = $request->all();
        $publicacion = Publicacion::create($publi);

        if ($request->hasFile('foto')) {
            $publicacion->addMediaFromRequest('foto')->toMediaCollection('images-publicaciones');
        }

        return response()->json(['success' => true, 'data' => $publicacion]);
    }


    // FUNCION PARA ACTUALIZAR PUBLICACIONES DE LA BD SEGUN EL ID QUE LE PASAS EN LA RUTA DE LA API
    public function update($id, Request $request)
    {
        $producto = Producto::find($id);

        $request->validate([
            'idCategoria' => 'required',
            'nombreProducto' => 'required',
            'linkProducto' => 'required',
            'valoracion' => 'required',
        ]);

        $producto->update($request->all());

        // Aquí debes pasar la valoración a la función sync
        $producto->publicaciones()->sync([$request->idPublicacion => ['valoracion' => $request->valoracion]]);

        return response()->json(['success' => true, 'data' => $producto]);
    }




    // FUNCION PARA ELIMINAR PUBLICACIONES DE LA BD SEGUN EL ID QUE LE PASAS EN LA RUTA DE LA API
    public function destroy($id)
    {
        $publi = Publicacion::find($id);

        // Primero obtenemos todos los productos asociados a la publicación
        $productos = $publi->productos;

        // Luego desasociamos los productos de la publicación
        $publi->productos()->detach();

        // Ahora podemos eliminar cada producto
        foreach ($productos as $producto) {
            $producto->delete();
        }

        // Desasociamos y eliminamos todas las otras relaciones
        $publi->megusta()->detach();
        $publi->comentarios()->delete();
        $publi->clics()->delete();

        // Y finalmente eliminamos la publicación
        $publi->delete();

        return response()->json(['succes' => true, 'data' => 'Publicación y productos asociados eliminados correctamente']);
    }
}
