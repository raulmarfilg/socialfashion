<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Http\Request;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $orderColumn = request('order_column', 'created_at');
        if (!in_array($orderColumn, ['id', 'name', 'created_at'])) {
            $orderColumn = 'created_at';
        }
        $orderDirection = request('order_direction', 'desc');
        if (!in_array($orderDirection, ['asc', 'desc'])) {
            $orderDirection = 'desc';
        }
        $users = User::when(request('search_id'), function ($query) {
            $query->where('id', request('search_id'));
        })
            ->when(request('search_title'), function ($query) {
                $query->where('name', 'like', '%' . request('search_title') . '%');
            })
            ->when(request('search_global'), function ($query) {
                $query->where(function ($q) {
                    $q->where('id', request('search_global'))
                        ->orWhere('name', 'like', '%' . request('search_global') . '%');
                });
            })
            ->orderBy($orderColumn, $orderDirection)
            ->paginate(50);

        return UserResource::collection($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return UserResource
     */
    public function store(StoreUserRequest $request)
    {
        $role = Role::find($request->role_id);
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
    
        if ($user->save()) {    
            if ($role) {
                $user->assignRole($role);
            }
            return new UserResource($user);
        }
    }
    


    // FUNCION PARA OBTENER LA IMAGEN DE PERFIL DE UN USUARIO
    public function getProfileImage($id)
    {
        // Busca al usuario por su ID
        $user = User::find($id);

        // Si no se encuentra al usuario, devuelve un error
        if (!$user) {
            return response()->json(['error' => 'User not found'], 404);
        }

        // Obtiene la imagen de perfil del usuario
        $profileImage = $user->media()->where('collection_name', 'images-perfil')->first();

        // Si el usuario no tiene una imagen de perfil, devuelve un error
        if (!$profileImage) {
            return response()->json(['error' => 'Profile image not found'], 404);
        }

        // Devuelve la imagen de perfil
        return response()->json($profileImage);
    }



    //SEGUIDORES Y SEGUIDOS DEL USUARIO
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return UserResource
     */
    public function show(User $user)
    {
        $user->load('roles', 'seguidores.usuario', 'seguidos.seguido');
        return new UserResource($user);
    }
    
    public function obtenerUsuarios()
    {
        $users = User::all();
    
        $users->each(function ($user) {
            if ($user->hasMedia('images-perfil')) {
                $user->imagen = $user->getFirstMedia('images-perfil')->getUrl();
            }
        });
    
        return $users;
    }



    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param User $user
     * @return UserResource
     */ 
    public function update(UpdateUserRequest $request, User $user)
    {
        $role = Role::find($request->role_id);

        $user->name = $request->name;
        $user->email = $request->email;
        if (!empty($request->password)) {
            $user->password = Hash::make($request->password) ?? $user->password;
        }

        if ($user->save()) {
            if ($role) {
                $user->syncRoles($role);
            }
            return new UserResource($user);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('user-delete');
        $user->delete();

        return response()->noContent();
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        $users = User::where('name', 'LIKE', "%{$search}%")->get();
        return UserResource::collection($users);
    }
    
}
