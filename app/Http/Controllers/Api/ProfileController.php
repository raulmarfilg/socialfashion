<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProfileRequest;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function updateImage(Request $request)
    {
        try {
            // Verificar si el usuario está autenticado
            $profile = Auth::user();

            if ($profile) {
                // Verificar si se proporcionó una imagen
                if ($request->hasFile('foto')) {
                    // Borrar cualquier imagen anterior del perfil
                    $profile->clearMediaCollection('images-perfil');

                    // Agregar la nueva imagen del perfil
                    $profile->addMediaFromRequest('foto')->toMediaCollection('images-perfil');

                    Log::info('Foto subida exitosamente');
                    return response()->json(['status' => 200, 'success' => true, 'message' => 'Imagen de perfil actualizada correctamente']);
                } else {
                    return response()->json(['status' => 400, 'success' => false, 'message' => 'No se proporcionó ninguna imagen']);
                }
            } else {
                return response()->json(['status' => 401, 'success' => false, 'message' => 'Usuario no autenticado']);
            }
        } catch (\Exception $e) {
            Log::error('Error al actualizar la imagen de perfil:', ['error' => $e->getMessage()]);
            return response()->json(['status' => 500, 'success' => false, 'message' => 'Error al actualizar la imagen de perfil']);
        }
    }



    public function update(UpdateProfileRequest $request)
    {
        try {
            $profile = Auth::user();
            $profile->name = $request->name;
            $profile->email = $request->email;

            if ($profile->save()) {
                return $this->successResponse($profile, 'Perfil actualizado correctamente');
            }
            return response()->json(['status' => 403, 'success' => false]);
        } catch (\Exception $e) {
            Log::error('Error al actualizar el perfil:', ['error' => $e->getMessage()]);
            return response()->json(['status' => 500, 'success' => false, 'message' => 'Error al actualizar el perfil']);
        }
    }


    public function user(Request $request)
    {
        $user = $request->user();

        $user->foto = $user->getFirstMediaUrl('images-perfil');

        return $this->successResponse($user, 'User found');
    }

    public function getUser($userId)
    {
        $user = User::find($userId);

        if (!$user) {
            return $this->errorResponse('User not found', 404); 
        }

        $user->foto = $user->getFirstMediaUrl('images-perfil');

        return $this->successResponse($user, 'User found'); 
    }


    public function show($username)
    {
        $user = User::where('name', $username)->first();
        if (!$user) {
            return response()->json(['message' => 'Usuario no encontrado'], 404);
        }

        // Obtener los datos del usuario
        $userData = [
            'name' => $user->name,
            'email' => $user->email,
            'id' => $user->id
        ];

        // Obtener los seguidores del usuario
        $seguidoresCount = $user->seguidores()->count();

        // Obtener los seguidos del usuario
        $seguidosCount = $user->seguidos()->count();

        // Puntos del usuario
        $puntos = $user->puntos;

        // Agregar los datos adicionales al arreglo
        $userData['seguidoresCount'] = $seguidoresCount;
        $userData['seguidosCount'] = $seguidosCount;
        $userData['puntos'] = $puntos;

        return response()->json($userData);
    }
}
