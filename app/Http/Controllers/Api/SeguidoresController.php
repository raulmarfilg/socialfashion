<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Seguidores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SeguidoresController extends Controller
{
    public function index($idUsuario){
        $seguidores = Seguidores::where('idUsuario', $idUsuario)->get()->toArray();
        return $seguidores;
    }

    public function followUser(Request $request, $username)
    {
        $user = $request->user();
        $followedUser = User::where('name', $username)->first();

        if (!$followedUser) {
            return response()->json(['message' => 'Usuario no encontrado'], 404);
        }

        // Verificar si el usuario ya sigue al usuario seguido
        if ($user->seguidos()->where('idSeguido', $followedUser->id)->exists()) {
            return response()->json(['message' => 'Ya sigues a este usuario'], 400);
        }

        // Seguir al usuario
        $user->seguidos()->attach($followedUser->id);

        return response()->json(['message' => 'Ahora sigues a ' . $followedUser->name]);
    }

    public function unfollowUser(Request $request, $username)
    {
        $user = $request->user();
        $followedUser = User::where('name', $username)->first();

        if (!$followedUser) {
            return response()->json(['message' => 'Usuario no encontrado'], 404);
        }

        // Dejar de seguir al usuario
        $user->seguidos()->detach($followedUser->id);

        return response()->json(['message' => 'Has dejado de seguir a ' . $followedUser->name]);
    }

    public function isFollowing(Request $request, $followerId, $followedId)
    {
        $isFollowing = User::find($followerId)->seguidos()->where('idSeguido', $followedId)->exists();

        return response()->json(['isFollowing' => $isFollowing]);
    }

    public function isCurrentUserFollowing(Request $request, $username)
    {
        $currentUser = $request->user();
        $followedUser = User::where('name', $username)->first();

        if (!$followedUser) {
            return response()->json(['message' => 'Usuario no encontrado'], 404);
        }

        // Verificar si el usuario actual sigue al usuario seguido
        $isFollowing = $currentUser->seguidos()->where('idSeguido', $followedUser->id)->exists();

        return response()->json(['isFollowing' => $isFollowing]);
    }

    public function followersCount($username)
    {
        $user = User::where('name', $username)->first();

        if (!$user) {
            return response()->json(['message' => 'Usuario no encontrado'], 404);
        }

        $followersCount = $user->seguidores()->count();

        return response()->json($followersCount);
    }

    public function followedCount($username)
    {
        $user = User::where('name', $username)->first();

        if (!$user) {
            return response()->json(['message' => 'Usuario no encontrado'], 404);
        }

        $followedCount = $user->seguidos()->count();

        return response()->json($followedCount);
    }
}
