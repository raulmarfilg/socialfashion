<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Producto;

class ProductoController extends Controller
{
    // FUNCION EN EL QUE SE AÑADE EL FILTRO SEGUN EL ID DE LA PUBLICACION ASI SE PUEDE FILTRAR POR PUBLICACION
    public function index($idPublicacion)
    {
        $producto = Producto::whereHas('publicaciones', function ($query) use ($idPublicacion) {
            $query->where('publicacions.id', $idPublicacion);
        })->with(['publicaciones' => function ($query) use ($idPublicacion) {
            $query->where('publicacions.id', $idPublicacion)->withPivot('valoracion');
        }])->get();
        return response()->json($producto);
    }



    public function store(Request $request)
    {
        $request->validate([
            'idPublicacion' => 'required',
            'idCategoria' => 'required',
            'nombreProducto' => 'required',
            'linkProducto' => 'required',
            'valoracion' => 'required',
        ]);

        $producto = Producto::create($request->all());

        // SE USA EL ATTACH EN VEZ DEL SYNC PARA CINSERVAR TODOS LOS PRODUCTOS SI HAY MAS DE UNO, YA QUE EL SYNC ACTUALIZA LA TABLA INTERMEDIA Y BORRABA LOS OTROS PRODUCTOS Y SOLO DEJABA UNO
        $producto->publicaciones()->attach($request->idPublicacion, ['valoracion' => $request->valoracion]);

        return response()->json(['success' => true, 'data' => $producto]);
    }



    // Función para actualizar un producto
    public function update($id, Request $request)
    {
        $producto = Producto::find($id);

        $request->validate([
            'idCategoria' => 'required',
            'nombreProducto' => 'required',
            'linkProducto' => 'required',
            'valoracion' => 'required',
        ]);

        $producto->update($request->all());

        // Aquí actualizamos la valoración en la tabla intermedia
        $producto->publicaciones()->updateExistingPivot($request->idPublicacion, ['valoracion' => $request->valoracion]);

        return response()->json(['success' => true, 'data' => $producto]);
    }
}
