<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Publicacion;
use App\Models\Clic;

class PuntosController extends Controller
{
    // FUNCION QUE SE USA PARA CREAR LOS CLICS Y SUMAR UN PUNTO AL USUARIO CUANDO SE DA AL LINK EN LA PUBLICACION
    public function incrementarPuntos(Request $request)
{
    $idUsuario = $request->input('idUsuario');
    $idPublicacion = $request->input('idPublicacion');

    $publicacion = Publicacion::find($idPublicacion);

    $idUsuarioPropietario = $publicacion->idUsuario;

    $clic = Clic::where('idUsuario', $idUsuario)->where('idPublicacion', $idPublicacion)->first();
    if (!$clic) {
        $user = User::find($idUsuarioPropietario);
        $user->puntos++;
        $user->save();

        $clic = new Clic;
        $clic->idUsuario = $idUsuario;
        $clic->idPublicacion = $idPublicacion;
        $clic->save();
    }

    return response()->json(['message' => 'Puntos incrementados correctamente']);
}
}
