<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// SE AÑADE EL NUEVO MODELO
use App\Models\Megusta;
use App\Models\Publicacion;


class MegustasController extends Controller
{
    public function index()
    {
        $megustas = Megusta::all()->toArray();
        return $megustas;
    }


    // FUNCION PARA AÑADIR MEGUSTA A LA BD
    public function store($id, $idUsuario)
    {
        $publicacion = Publicacion::find($id);
        $existingLike = $publicacion->megusta()->where('idUsuario', $idUsuario)->first();
    
        if ($existingLike) {
            return response()->json(['success' => false, 'message' => 'Ya has dado "me gusta" a esta publicación']);
        }
    
        $publicacion->megusta()->attach($idUsuario, ['idPublicacion' => $id, 'idUsuario' => $idUsuario]);
    
        return response()->json(['success' => true, 'data' => 'Me gusta añadido correctamente']);
    }
    


    // FUNCION PARA ELIMINAR EL MEGUSTA DE LA BD SEGUN EL ID QUE LE PASAS EN LA RUTA DE LA API
    public function destroy($idPublicacion, $idUsuario){
        $publicacion = Publicacion::find($idPublicacion);
        $existingLike = $publicacion->megusta()->where('idUsuario', $idUsuario)->first();
    
        if ($existingLike) {
            $publicacion->megusta()->detach($idUsuario);
            return response()->json(['success' => true, 'message' => 'Has quitado tu "me gusta" de esta publicación']);
        } else {
            return response()->json(['success' => false, 'message' => 'No has dado "me gusta" a esta publicación']);
        }
    }
}
