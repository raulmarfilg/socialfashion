<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comentario;

class ComentariosController extends Controller
{
    public function index($idPublicacion){
        $comentarios = Comentario::where('idPublicacion', $idPublicacion)->with('usuario')->get();
    return response()->json($comentarios);
    }


    // FUNCION PARA AÑADIR COMENTARIO A LA PUBLICACION
    public function store($id, $idUsuario, $comentario){
        $nuevoComentario = Comentario::create([
            'idPublicacion' => $id, 
            'idUsuario' =>  $idUsuario,
            'comentario' => $comentario
        ]);
    
        // Carga la relación usuario.media
        $nuevoComentario->load('usuario.media');
    
        return response()->json(['success' => true, 'data' => $nuevoComentario]);
    }


    // // FUNCION PARA ACTUALIZAR PUBLICACIONES DE LA BD SEGUN EL ID QUE LE PASAS EN LA RUTA DE LA API
    // public function update($id, Request $request){
    //     $publi = Comentario::find($id);

    //     $request->validate([
    //         'idUsuario' => 'required',
    //         'idPublicacion' => 'required',
    //         'comentario' => 'required',
    //     ]);

    //     $dataToUpdate = $request->all();
    //     $publi->update($dataToUpdate);

    //     return response()->json(['succes' => true, 'data' => $publi]);
    // }



    // // FUNCION PARA ELIMINAR PUBLICACIONES DE LA BD SEGUN EL ID QUE LE PASAS EN LA RUTA DE LA API
    // public function destroy($id){
    //     $publi = Comentario::find($id);
    //     $publi->delete();

    //     return response()->json(['succes' => true, 'data' => 'Comentario eliminado correctamente']);
    // }
}
