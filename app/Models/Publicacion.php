<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Publicacion extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'idUsuario',
        'dePago',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'idUsuario');
    }

    public function megusta()
    {
        return $this->belongsToMany(User::class, 'megustas', 'idPublicacion', 'idUsuario');
    }

    public function comentarios()
    {
        return $this->hasMany(Comentario::class, 'idPublicacion');
    }

    public function clics()
    {
        return $this->hasMany(Clic::class, 'idPublicacion');
    }

    public function productos()
    {
        return $this->belongsToMany(Producto::class, 'producto_publicacion', 'idPublicacion', 'idProducto')->withPivot('valoracion');
    }
        



    // public function megustaCount()
    // {
    //     return $this->users()->count();
    // }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('images/exercises')
            ->useFallbackUrl('/images/placeholder.jpg')
            ->useFallbackPath(public_path('/images/placeholder.jpg'));
    }
}
