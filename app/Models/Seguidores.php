<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seguidores extends Model
{
    use HasFactory;

    protected $fillable = [
        'idUsuario',
        'idSeguido'
    ];

    public function usuario()
    {
        return $this->belongsToMany(User::class, 'seguidores', 'idSeguido', 'idUsuario');
    }

    public function seguido()
    {
        return $this->belongsToMany(User::class, 'seguidores', 'idUsuario', 'idSeguido');
    }
}
