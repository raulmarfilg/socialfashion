<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clic extends Model
{
    use HasFactory;

    protected $fillable = [
        'idUsuario',
        'idPublicacion'
    ];

    public function usuario()
    {
        return $this->belongsTo(User::class, 'idUsuario');
    }

    public function publicacion()
    {
        return $this->belongsTo(Publicacion::class, 'idPublicacion');
    }
}