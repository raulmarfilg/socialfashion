<?php

namespace App\Models;

use App\Notifications\UserResetPasswordNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class User extends Authenticatable implements HasMedia
{
    use Notifiable, HasApiTokens, HasRoles, InteractsWithMedia, HasFactory;

    protected $fillable = [
        'name',
        'email',
        'password',
        'puntos'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPasswordNotification($token));
    }

    public function seguidores()
    {
        return $this->belongsToMany(User::class, 'seguidores', 'idSeguido', 'idUsuario');
    }
    
    public function seguidos()
    {
        return $this->belongsToMany(User::class, 'seguidores', 'idUsuario', 'idSeguido');
    }

    public function megusta()
    {
        return $this->belongsToMany(Publicacion::class, 'megustas', 'idUsuario', 'idPublicacion');
    }
}
