<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    protected $fillable = [
        'idPublicacion',
        'idCategoria',
        'nombreProducto',
        'linkProducto'
    ];

    public function publicaciones()
    {
        return $this->belongsToMany(Publicacion::class, 'producto_publicacion', 'idProducto', 'idPublicacion')->withPivot('valoracion');
    }
    
}
