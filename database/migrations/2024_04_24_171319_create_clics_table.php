<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClicsTable extends Migration
{
    public function up()
    {
        Schema::create('clics', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idUsuario')->constrained('users');
            $table->foreignId('idPublicacion')->constrained('publicacions');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('clics');
    }
}
