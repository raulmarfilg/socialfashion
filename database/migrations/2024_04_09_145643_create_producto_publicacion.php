<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('producto_publicacion', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idProducto')->constrained('productos', 'id');
            $table->foreignId('idPublicacion')->constrained('publicacions', 'id');
            $table->integer('valoracion')->nullable();
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('producto_publicacion');
    }
};
