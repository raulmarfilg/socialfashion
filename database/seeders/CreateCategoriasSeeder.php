<?php

namespace Database\Seeders;

use App\Models\Categoria;
use Illuminate\Database\Seeder;

class CreateCategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['nombreCategoria' => 'Parte superior', 'descripcion' => 'Descripción de Parte superior', 'icono' => '/images/parte_superior.svg'],
            ['nombreCategoria' => 'Parte inferior', 'descripcion' => 'Descripción de Parte inferior', 'icono' => '/images/parte_inferior.svg'],
            ['nombreCategoria' => 'Calzado', 'descripcion' => 'Descripción de Calzado', 'icono' => '/images/calzado.svg'],
            ['nombreCategoria' => 'Accesorios', 'descripcion' => 'Descripción de Accesorios', 'icono' => '/images/accesorios.svg'],
        ];

        foreach ($categories as $category) {
            Categoria::create($category);
        }
    }
}
