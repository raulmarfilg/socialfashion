<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Nacho',
            'email' => 'nacho@gmail.com',
            'password' => bcrypt('12345678'),
            'puntos' => 0,
            'role' => 'user'
        ]);

        // Añadir la imagen por defecto
        $user->addMedia(public_path('images/userDefecto.png'))->toMediaCollection('images-perfil');

        $user = User::create([
            'name' => 'Raul',
            'email' => 'raulmarfilg@gmail.com',
            'password' => bcrypt('12345678'),
            'puntos' => 0,
            'role' => 'user'
        ]);

        // Añadir la imagen por defecto
        $user->addMedia(public_path('images/userDefecto2.png'))->toMediaCollection('images-perfil');
    }
}
