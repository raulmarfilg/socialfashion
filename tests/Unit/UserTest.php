<?php 
namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class UserTest extends TestCase
{
    use RefreshDatabase;

    
    // USA EL FACTORY PARA CREAR UN USUARIO AUTENTICADO PARA PODER REALIZAR UNA LLAMADA A LA API DE CREAR USUARIO Y QUE NO HAGA REDIRECT POR NO ESTAR AUTORIZADO
    /** @test */
    public function it_can_create_a_user()
    {
        $user = User::factory()->create();

        $userData = [
            'name' => 'Test User',
            'email' => 'testuser@example.com',
            'password' => 'password',
        ];

        $response = $this->actingAs($user)->post('/api/users', $userData);

        $response->assertStatus(201);

        $this->assertDatabaseHas('users', [
            'name' => $userData['name'],
            'email' => $userData['email'],
        ]);
    }

    // USA EL FACTORY PARA CREAR UN USUARIO AUTENTICADO PARA PODER REALIZAR UNA LLAMADA A LA API DE CREAR USUARIO Y QUE NO HAGA REDIRECT POR NO ESTAR AUTORIZADO,
    // CREA UN TOKKEN PARA ENVIARLO EN LA SOLICITUD Y OBTENER EL AUTH DEVIDO A QUE LA API ESTA DENTRO DE UN MIDDLEWARE DEL SANCTUM, Y VALIDA QUE NO SE PUEDA CREAR UN USUARIO SIN LOS DATOS REQUERIDOS

    /** @test */
    public function it_cannot_create_a_user_without_required_data()
    {
        $user = User::factory()->create();

        $token = $user->createToken('test-token')->plainTextToken;

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])->json('POST', '/api/users', [
            'name' => '', 
            'email' => '',
            'password' => '',
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['name', 'email', 'password']);
    }


    // VALIDA QUE SE PUEDA ELIMINAR UN USUARIO PASANDOLE LOS DATOS REQUERIDOS, PARA ESO CREA OTRO USUARIO QUE TENGA EL PERMISO DE ELIMINAR USUARIOS Y EL TOKEN PARA NO TENER PROBLEMAS CON EL MIDDLEWARE
    /** @test */
    public function it_can_delete_a_user()
    {
        $user = User::factory()->create();

        Permission::create(['name' => 'user-delete']);

        $user->givePermissionTo('user-delete');

        $token = $user->createToken('test-token')->plainTextToken;

        $userToDelete = User::factory()->create();

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])->json('DELETE', '/api/users/'.$userToDelete->id);

        $response->assertStatus(204);

        $this->assertDatabaseMissing('users', [
            'id' => $userToDelete->id,
        ]);
    }


    // VALIDA QUE NO SE PUEDA ELIMINAR UN USUARIO SI SE ENVIA UN DATO ERRONEO, EN ESTE CASO EL USUARIO A ELIMINAR
    /** @test */
    public function it_cannot_delete_a_user_without_valid_id()
    {
        $user = User::factory()->create();

        $token = $user->createToken('test-token')->plainTextToken;

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])->json('DELETE', '/api/users/999999');

        $response->assertStatus(404);
    }


    //CREA UN USUARIO Y LO ACTUALIZA PASANDOLE DATOS A ACTUALIZAR
    /** @test */
    public function it_can_update_a_user()
    {
        $user = User::factory()->create();

        $token = $user->createToken('test-token')->plainTextToken;

        $userToUpdate = User::factory()->create();

        $updateData = [
            'name' => 'Updated User',
            'email' => 'updateduser@example.com',
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])->json('PUT', '/api/users/'.$userToUpdate->id, $updateData);

        $response->assertStatus(200);

        $this->assertDatabaseHas('users', [
            'id' => $userToUpdate->id,
            'name' => $updateData['name'],
            'email' => $updateData['email'],
        ]);
    }


    //CREA UN USUARIO Y COMPRUEVA QUE NO LO ACTUALIZA PASANDOLE DATOS INCORRECTOS
    /** @test */
    public function it_cannot_update_a_user_with_invalid_data()
    {
        $user = User::factory()->create();

        $token = $user->createToken('test-token')->plainTextToken;

        $userToUpdate = User::factory()->create();

        $invalidData = [
            'name' => '',
            'email' => 'noesunemail',
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])->json('PUT', '/api/users/'.$userToUpdate->id, $invalidData);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['name', 'email']);
    }
}