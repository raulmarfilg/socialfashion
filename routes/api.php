<?php

use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\PermissionController;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\ExerciseController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\CategoriasController;
use App\Http\Controllers\Api\SeguidoresController;
use App\Http\Controllers\Api\PuntosController;


// AÑADIDO CONTROLADOR DE PUBLICACIONES
use App\Http\Controllers\Api\PublicacionesController;
use App\Http\Controllers\Api\MegustasController;
use App\Http\Controllers\Api\ComentariosController;
use App\Http\Controllers\Api\ProductoController;

use App\Http\Controllers\Auth\ResetPasswordController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\ForgotPasswordController;

Route::post('forget-password', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('forget.password.post');
Route::post('reset-password', [ResetPasswordController::class, 'reset'])->name('password.reset');


// RUTAS PUNTOS Y CLICS
Route::post('clics', [PuntosController::class, 'incrementarPuntos']);

// RUTAS SEGUIDORES
Route::get('seguidores/{idUsuario}', [SeguidoresController::class, 'index']);
Route::middleware('auth:sanctum')->group(function () {
    Route::post('follow/{username}', [SeguidoresController::class, 'followUser']);
    Route::post('unfollow/{username}', [SeguidoresController::class, 'unfollowUser']);
    Route::get('is-following/{username}', [SeguidoresController::class, 'isCurrentUserFollowing']);
    Route::get('/followers-count/{username}', [SeguidoresController::class, 'followersCount']);
    Route::get('/followed-count/{username}', [SeguidoresController::class, 'followedCount']);
    

});

// RUTA PRODUCTOS
Route::get('productos/{idPublicacion}', [ProductoController::class, 'index']);
Route::post('productos/', [ProductoController::class, 'store']);
Route::put('productos/update/{id}', [ProductoController::class, 'update']);
// RUTAS DE LAS CATEGORIAS
Route::get('categorias', [CategoriasController::class, 'index']);
// RUTAS DE LAS FUNCIONES CREADAS EN EL PUBLICACIONESCONTROLLER
Route::get('publicacions', [PublicacionesController::class, 'index']);
Route::post('publicacions/', [PublicacionesController::class, 'store']);
// Revisar de cambiar el put por el post para que se puedan modificar las imagenes
Route::put('publicacions/update/{id}', [PublicacionesController::class, 'update']);
Route::delete('publicacions/{id}', [PublicacionesController::class, 'destroy']);

// RUTAS DE LAS FUNCIONES CREADAS EN EL MEGUSTASCONTROLLER
Route::get('publicacions/{id}/megustas', [MegustasController::class, 'index']);
Route::post('publicacions/{id}/megustas/{idUsuario}', [MegustasController::class, 'store']);
Route::delete('publicacions/{id}/megustas/{idUsuario}', [MegustasController::class, 'destroy']);
//

// RUTAS DE LAS FUNCIONES CREADAS EN EL COMENTARIOSCONTROLLER
Route::get('publicacions/{id}/comentarios', [ComentariosController::class, 'index']);
Route::post('publicacions/{id}/comentarios/{idUsuario}/{comentario}', [ComentariosController::class, 'store']);
// Route::put('publicacions/update/{id}',[ComentariosController::class, 'update']);
// Route::delete('publicacions/{id}/comentarios/{idUsuario}',[ComentariosController::class, 'destroy']);
//

// RUTA PARA OBTENER IMG PERFIL USER
Route::get('user/{id}/profile-image', [UserController::class, 'getProfileImage']);

// RUTA DEL SEARCH DEL EXPLORAR
Route::get('/search', [UserController::class, 'search']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::apiResource('users', UserController::class);
    Route::apiResource('posts', PostController::class);
    Route::apiResource('categories', CategoryController::class);
    Route::apiResource('roles', RoleController::class);
    //Route::apiResource('exercises', ExerciseController::class);
    Route::post('exercises/', [ExerciseController::class, 'store']); //Guardar
    Route::get('exercises', [ExerciseController::class, 'index']); //Listar
    Route::get('exercises/{exercise}', [ExerciseController::class, 'show']); //Mostrar
    Route::post('exercises/update/{id}', [ExerciseController::class, 'update']); //Editar

    Route::get('role-list', [RoleController::class, 'getList']);
    Route::get('role-permissions/{id}', [PermissionController::class, 'getRolePermissions']);
    Route::put('/role-permissions', [PermissionController::class, 'updateRolePermissions']);
    Route::apiResource('permissions', PermissionController::class);
    Route::get('category-list', [CategoryController::class, 'getList']);
    Route::get('/user', [ProfileController::class, 'user']);
    Route::put('/user', [ProfileController::class, 'update']);
    Route::post('/userFoto', [ProfileController::class, 'updateImage']);
    Route::get('/user/{userId}', [ProfileController::class, 'getUser']);

    //ADMIN USERS
    Route::get('/all-users', [UserController::class, 'obtenerUsuarios']);
    Route::put('/users/{id}', [UserController::class, 'update']);
    Route::delete('/users/{id}', [UserController::class, 'destroy']);



    Route::get('abilities', function (Request $request) {
        return $request->user()->roles()->with('permissions')
            ->get()
            ->pluck('permissions')
            ->flatten()
            ->pluck('name')
            ->unique()
            ->values()
            ->toArray();
    });
});

Route::get('/profile/{username}', [ProfileController::class, 'show']);


Route::get('category-list', [CategoryController::class, 'getList']);
Route::get('get-posts', [PostController::class, 'getPosts']);
Route::get('get-category-posts/{id}', [PostController::class, 'getCategoryByPosts']);
Route::get('get-post/{id}', [PostController::class, 'getPost']);
