import Cookies from 'js-cookie'
import store from "../store";



const AuthenticatedLayout = () => import('../layouts/Authenticated.vue')
const AuthenticatedAdminLayout = () => import('../layouts/AuthenticatedAdmin.vue')
const GuestLayout = () => import('../layouts/Guest.vue');
const InvitadoLayout = () => import('../layouts/Invitado.vue');
const LoginView = () => import('../views/login/Login.vue');


const PostsIndex = () => import('../views/admin/posts/Index.vue');
const PostsCreate = () => import('../views/admin/posts/Create.vue');
const PostsEdit = () => import('../views/admin/posts/Edit.vue');
const ExercisesIndex = () => import('../views/admin/exercises/Index.vue');
const ExercisesCreate = () => import('../views/admin/exercises/Create.vue');
const ExercisesEdit = () => import('../views/admin/exercises/Edit.vue');
//NUEVAS RUTAS AÑADIDAS
const PublicacionesList = () => import('../views/inicio/Index.vue');
const PublicacionesCreate = () => import('../views/inicio/Create.vue');
//NUEVAS RUTAS AÑADIDAS ADMIN
const PublicacionesAdminList = () => import('../views/admin/index.vue');
const PublicacionesAdminCreate = () => import('../views/admin/create/CreateAdminPubli.vue');
const UserAdminCreate = () => import('../views/admin/create/CreateAdminUser.vue');

// Controlador de eventos para el cierre de sesión
store.watch((state) => state.auth.authenticated, (isAuthenticated) => {
    if (!isAuthenticated) {
        // Si el usuario cierra la sesión, borra la variable 'first_login' de sessionStorage
        sessionStorage.removeItem('first_login');
        // Redirige al usuario a la página de inicio de sesión
        // router.push({ name: 'auth.login' });
    }
});


function requireLogin(to, from, next) {
    const isLogin = !!store.state.auth.authenticated;

    if (isLogin) {
        // Verificar si el usuario es un administrador o un usuario normal
        const isAdmin = store.state.auth.user.role === 'admin';
        // Redirigir al usuario según su rol
        if (isAdmin) {
            if (to.path !== '/admin') {
                next('/admin');
            } else {
                next();
            }
        } else {
            if (to.path !== '/user/inicio') {
                next('/user/inicio');
            } else {
                next();
            }
        }
    } else {
        next('/login');
    }
}


// Función para permitir acceso a invitados
function guest(to, from, next) {
    const isLogin = !!store.state.auth.authenticated;

    if (!isLogin) {
        // Permitir acceso si el usuario no está autenticado
        next();
    } else {
        // Redirigir al usuario según su rol (administrador o usuario normal)
        const isAdmin = store.state.auth.user.role === 'admin';
        next(isAdmin ? '/admin' : '/user/inicio');
    }
}

// Función para permitir acceso a invitados
function invitado(to, from, next) {
    const isLogin = !!store.state.auth.authenticated;

    if (!isLogin || to.name === 'explorarInvitado.index') {
        // Permitir acceso si el usuario no está autenticado, o si la ruta es 'explorarInvitado.index'
        next();
    } else {
        // Redirigir al usuario según su rol (administrador o usuario normal)
        const isAdmin = store.state.auth.user.role === 'admin';
        next(isAdmin ? '/admin' : '/user/inicio');
    }
}


export default [
    {
        path: '/',
        component: GuestLayout,
        beforeEnter: invitado,
        children: [

            {
                path: '/',
                name: 'home',
            },
            {
                path: 'register',
                name: 'auth.register',
                component: () => import('../views/register/index.vue'),
                beforeEnter: guest,
            },
            {
                path: 'forgot-password',
                name: 'auth.forgot-password',
                component: () => import('../views/auth/passwords/Email.vue'),
                beforeEnter: guest,
            },
            {
                path: 'reset-password/:token',
                name: 'auth.reset-password',
                component: () => import('../views/auth/passwords/Reset.vue'),
                beforeEnter: guest,
            },
            {
                path: 'login',
                name: 'auth.login',
                component: LoginView,
                beforeEnter: guest
            },
            {
                path: '',
                name: 'explorarInvitado.index',
                component: () => import('../views/explorar/explorarInvitado.vue'),
            },
        ]
    },
    {
        path: '/explorarInvitado',
        component: InvitadoLayout,
        children: [

            {
                path: '',
                name: 'explorarInvitado.index',
                component: () => import('../views/explorar/explorarInvitado.vue'),
            },
        ]
    },
    {
        path: '/admin',
        component: AuthenticatedAdminLayout,
        beforeEnter: requireLogin,
        meta: { breadCrumb: 'Dashboard' },
        children: [
            {
                name: 'admin.index',
                path: '',
                component: () => import('../views/admin/index.vue'),
                meta: { breadCrumb: 'Admin' }
            },
            {
                path: 'explorar',
                name: 'explorar.index',
                component: () => import('../views/explorar/index.vue'),
                meta: { breadCrumb: 'Explorar' }
            },
            {
                name: 'profile.index',
                path: 'profile',
                component: () => import('../views/profile/index.vue'),
                meta: { breadCrumb: 'Profile' }
            },
            {
                name: 'profile.UserProfile',
                path: '/profile/:username',
                component: () => import('../views/profile/UserProfile.vue'),
            },
            {
                name: 'profile.updateprofile',
                path: 'profile',
                component: () => import('../views/profile/updateprofile.vue'),
                meta: { breadCrumb: 'Profile' }
            },
            {
                name: 'posts.index',
                path: 'posts',
                component: PostsIndex,
                meta: { breadCrumb: 'Posts' }
            },
            {
                name: 'posts.create',
                path: 'posts/create',
                component: PostsCreate,
                meta: { breadCrumb: 'Add new post' }
            },
            {
                name: 'posts.edit',
                path: 'posts/edit/:id',
                component: PostsEdit,
                meta: { breadCrumb: 'Edit post' }
            },

            {
                name: 'panel',
                path: 'panel',
                meta: { breadCrumb: 'Panel' },
                children: [
                    {
                        name: 'panel.index',
                        path: '',
                        component: PublicacionesAdminList,
                        meta: { breadCrumb: 'View' }
                    },
                    {
                        name: 'panel.createPubli',
                        path: 'create',
                        component: PublicacionesAdminCreate,
                        meta: {
                            breadCrumb: 'Crea una publicacion',
                            linked: false,
                        }
                    },
                    {
                        name: 'panel.createUser',
                        path: 'create',
                        component: UserAdminCreate,
                        meta: {
                            breadCrumb: 'Crea un usuario',
                            linked: false,
                        }
                    },
                ]
            },

            {
                name: 'exercises',
                path: 'exercises',
                meta: { breadCrumb: 'Exercises' },
                children: [
                    {
                        name: 'exercises.index',
                        path: '',
                        component: ExercisesIndex,
                        meta: { breadCrumb: 'View' }
                    },
                    {
                        name: 'exercises.create',
                        path: 'create',
                        component: ExercisesCreate,
                        meta: {
                            breadCrumb: 'Add new exercise',
                            linked: false,
                        }
                    },
                    {
                        name: 'exercises.edit',
                        path: 'edit/:id',
                        component: ExercisesEdit,
                        meta: {
                            breadCrumb: 'Edit exercise',
                            linked: false,
                        }
                    }
                ]
            },

            {
                name: 'categories',
                path: 'categories',
                meta: { breadCrumb: 'Categories' },
                children: [
                    {
                        name: 'categories.index',
                        path: '',
                        component: () => import('../views/admin/categories/Index.vue'),
                        meta: { breadCrumb: 'View category' }
                    },
                    {
                        name: 'categories.create',
                        path: 'create',
                        component: () => import('../views/admin/categories/Create.vue'),
                        meta: {
                            breadCrumb: 'Add new category',
                            linked: false,
                        }
                    },
                    {
                        name: 'categories.edit',
                        path: 'edit/:id',
                        component: () => import('../views/admin/categories/Edit.vue'),
                        meta: {
                            breadCrumb: 'Edit category',
                            linked: false,
                        }
                    }
                ]
            },

            {
                name: 'permissions',
                path: 'permissions',
                meta: { breadCrumb: 'Permisos' },
                children: [
                    {
                        name: 'permissions.index',
                        path: '',
                        component: () => import('../views/admin/permissions/Index.vue'),
                        meta: { breadCrumb: 'Permissions' }
                    },
                    {
                        name: 'permissions.create',
                        path: 'create',
                        component: () => import('../views/admin/permissions/Create.vue'),
                        meta: {
                            breadCrumb: 'Create Permission',
                            linked: false,
                        }
                    },
                    {
                        name: 'permissions.edit',
                        path: 'edit/:id',
                        component: () => import('../views/admin/permissions/Edit.vue'),
                        meta: {
                            breadCrumb: 'Permission Edit',
                            linked: false,
                        }
                    }
                ]
            },
            //TODO Organizar rutas
            {
                name: 'roles.index',
                path: 'roles',
                component: () => import('../views/admin/roles/Index.vue'),
                meta: { breadCrumb: 'Roles' }
            },
            {
                name: 'roles.create',
                path: 'roles/create',
                component: () => import('../views/admin/roles/Create.vue'),
                meta: { breadCrumb: 'Create Role' }
            },
            {
                name: 'roles.edit',
                path: 'roles/edit/:id',
                component: () => import('../views/admin/roles/Edit.vue'),
                meta: { breadCrumb: 'Role Edit' }
            },
            {
                name: 'users.index',
                path: 'users',
                component: () => import('../views/users/Index.vue'),
                meta: { breadCrumb: 'Users' }
            },
            {
                name: 'users.create',
                path: 'users/create',
                component: () => import('../views/users/Create.vue'),
                meta: { breadCrumb: 'Add New' }
            },
            {
                name: 'users.edit',
                path: 'users/edit/:id',
                component: () => import('../views/users/Edit.vue'),
                meta: { breadCrumb: 'User Edit' }
            },
            // añadido 
            {
                name: 'inicio',
                path: 'inicio',
                meta: { breadCrumb: 'Publicaciones' },
                children: [
                    {
                        name: 'inicio.index',
                        path: '',
                        component: PublicacionesList,
                        meta: { breadCrumb: 'Listado publicaciones' }
                    },
                    {
                        name: 'inicio.create',
                        path: 'create',
                        component: PublicacionesCreate,
                        meta: { breadCrumb: 'Crear publicación' }
                    },
                    {
                        name: 'publicationDetails',
                        path: '/inicio/:id',
                        component: () => import('@/views/inicio/index.vue')
                    }

                ]
            },
        ]
    },

    {
        path: '/user',
        component: AuthenticatedLayout,
        beforeEnter: requireLogin,
        children: [
            {
                name: 'users.index',
                path: 'users',
                component: () => import('../views/users/Index.vue'),
                meta: { breadCrumb: 'Users' }
            },
            {
                path: 'explorar',
                name: 'explorar.index',
                component: () => import('../views/explorar/index.vue'),
                meta: { breadCrumb: 'Explorar' }
            },
            {
                name: 'profile.index',
                path: 'profile2',
                component: () => import('../views/profile/index.vue'),
                meta: { breadCrumb: 'Profile' }
            },
            {
                name: 'profile.UserProfile',
                path: '/profile/:username',
                component: () => import('../views/profile/UserProfile.vue'),
            },
            {
                name: 'profile.updateprofile',
                path: 'profile/update',
                component: () => import('../views/profile/updateprofile.vue'),
                meta: { breadCrumb: 'Profile' }
            },
            {
                name: 'posts.index',
                path: 'posts',
                component: PostsIndex,
                meta: { breadCrumb: 'Posts' }
            },
            {
                name: 'posts.create',
                path: 'posts/create',
                component: PostsCreate,
                meta: { breadCrumb: 'Add new post' }
            },
            {
                name: 'posts.edit',
                path: 'posts/edit/:id',
                component: PostsEdit,
                meta: { breadCrumb: 'Edit post' }
            },
            //RUTA INICIO
            {
                name: 'inicio',
                path: 'inicio',
                meta: { breadCrumb: 'Publicaciones' },
                children: [
                    {
                        name: 'inicio.index',
                        path: '',
                        component: PublicacionesList,
                        meta: { breadCrumb: 'Listado publicaciones' }
                    },
                    {
                        name: 'inicio.create',
                        path: 'create',
                        component: PublicacionesCreate,
                        meta: { breadCrumb: 'Crear publicación' }
                    },
                    {
                        name: 'publicationDetails',
                        path: '/inicio/:id',
                        component: () => import('@/views/inicio/index.vue')
                    }

                ]
            },

        ]
    },
    {
        path: "/:pathMatch(.*)*",
        name: 'NotFound',
        component: () => import("../views/errors/404.vue"),
    },
];
