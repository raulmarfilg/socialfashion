# SocialFashion

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/raulmarfilg/socialfashion.git
git branch -M main
git push -uf origin main

```

## Descripción

La web SOCIAL FASHION es una red social de moda, combinando interfaces de las redes más populares como tiktok e instagram, donde los usuarios pueden compartir y descubrir productos de moda. Los propios usuarios establecen los productos a través de sus publicaciones, proporcionando enlaces para comprar los artículos presentados. Además, los usuarios reciben puntos por cada enlace en el que se hace clic, fomentando la interacción en la plataforma. En resumen, SOCIAL FASHION es una comunidad de moda interactiva la cual los mismos usuarios serán nuestros propios vendedores y clientes.


# Características

- ✅ Laravel 10
- ✅ Vue 3
- ✅ VueRouter + Vuex
- ✅ Vue i18n Multi Idioma
- ✅ Iniciar sesión
- ✅ Login
- ✅ Panel de administración
- ✅ Gestión de publicaciones
- ✅ Interacciones de usuarios en publicaciones
- ✅ Filtros de publicaciones
- ✅ Gestión de usuarios
- ✅ Verificación de correo electrónico
- ✅ Blog de Frontend
- ✅ Boostrap 5
- ✅ Verificación de correo electrónico

## Requisitos

- PHP 8.0+
- Composer
- Node.js 14+


## Como usar
### Clonar Repositorio 

```bash
git clone https://gitlab.com/raulmarfilg/socialfashion.git
```

### Instalar vía Composer

```bash
composer install
```

### Copiar el fichero .env.example  a .env edita las credenciales y la url


### Generar Application Key

```bash
php artisan key:generate
```

### Migrar base de datos

```bash
php artisan migrate
```

### Lanzar Seeders

```bash
php artisan db:seed
```

### Instalar las dependencias de Node

```bash
npm install

npm run dev
```
### Lanzar a producción

```bash
npm run build or yarn build
```

## Implementación link Storage

```bash
php artisan storage:link
```

## Instalación de Bootstrap

```bash
npm install bootstrap
```

## Instalación de Iconos de Bootstrap

```bash
npm install bootstrap-icons
```

## Instalación de Axios

```bash
npm install axios

```
## Instalación de SweetAlert2

```bash
npm install sweetalert2
```
## Instalación de Font Awesome

```bash
npm install @fortawesome/fontawesome-free
```
## Instalación de Vue Router

```bash
npm install vue-router
```
